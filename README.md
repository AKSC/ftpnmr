# FTPNMR
## Description
Internal script for NMR data transfer.

## Installation
### Basic
1. Install the package with `pip`:
```sh
pip3 install .
```
2. Copy the `ftpnmr.yml` in `config` to your home and adjust the configuration:
```sh
cp config/ftpnmr.yml.example ~/ftpnmr.yml
```
3. Configure `ftpnmr.yml` for your needs

### Add as service to systemd
4. Copy the `ftpnmr.service` in `systemd` to `/etc/systemd/system` and adjust the configuration if needed:
```sh
cp systemd/ftpnmr.service /etc/systemd/system/.
```
5. Enable the service with `systemctl enable ftpnmr.service`

## Usage
| Task | Command Line |
| --- | --- |
| Simple execution | `python3 -m ftpnmr` |
| Start as service | `systemctl start ftpnmr.service` |
| Stop service | `systemctl stop ftpnmr.service` |
| Get log of service | `journalctl -u ftpnmr.service` |

## Support
_TBD_

## Roadmap
_TBD_

## Contributing
_TBD_

## Authors and acknowledgment
_TBD_

## License
MIT License

## Project status
_TBD_
