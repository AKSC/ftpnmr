#!/usr/bin/env python3
from setuptools import setup

setup(
    name='ftpnmr',
    version='0.5.3',
    description='Internal script for NMR data transfer',
    author='Sebastian Wellig',
    license='MIT',
    url='https://git.rwth-aachen.de/AKSC/ftpnmr',
    packages=['ftpnmr'],
    python_requires=">=3.6.*",
    install_requires=[i.strip() for i in open('requirements.txt')]
)
