#!/usr/bin/env python3
import time
import json
import yaml
import shutil
import logging
import subprocess
from pathlib import Path
from datetime import datetime
from multiprocessing import Process
from .__init__ import __version__

with open(Path.home() / 'ftpnmr.yml', 'r') as stream:
    config = yaml.safe_load(stream)

POLL_TIMEOUT = config.get('poll_timeout', 1)
POLL_TOLLERANCE = config.get('poll_tollerance', 10)
SRC_ROOT_LIST = config['source_roots'] 
TARGET_ROOT = Path(config['target_root'])
GROUP_MAPPINGS = config['group_mappings']
LOGGING_LEVEL = config.get('logging_level', 'WARNING')

try:
    logging.basicConfig(level=getattr(logging, LOGGING_LEVEL))
except AttributeError:
    raise RuntimeError(f'logging_level "{LOGGING_LEVEL}" is not valid')


def mount(cfg):
    if isinstance(cfg, dict):
        cmd = ['mount']
        if 'options' in cfg:
            cmd.extend(cfg['options'].split())
        if 'device' in cfg:
            cmd.append(cfg['device'])
        mountpoint = cfg['mountpoint']
        cmd.append(mountpoint)
        try:
            subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            logging.warning(e.output.decode('ascii'))
        return Path(mountpoint)
    else:
        return Path(cfg)


class SpectHandler:
    def __init__(self, inp_root_path, out_root_path, poll_timeout=1):
        self.inp_root_path = inp_root_path
        self.out_root_path = out_root_path
        self.spect_name = inp_root_path.name
        self.poll_timeout = poll_timeout
        self.poll_time_file = out_root_path / f'.{self.spect_name}_times.json'
        self.logger = logging.getLogger(self.spect_name)
        try:
            self.inp_root_path.mkdir(exist_ok=True)
            self.out_root_path.mkdir(exist_ok=True, parents=True)
        except OSError as e:
            raise RuntimeError(f'Failed to initialize handler: {e}')

    def copy_data(self, inp_path, out_path):
        try:
            # if path is folder or inside a folder, zip the folder
            if inp_path.is_dir():
                shutil.make_archive(out_path, 'zip', inp_path)
            else:
                shutil.copy(inp_path, out_path)
            self.logger.debug(f'Copying: {inp_path} -> {out_path}')
        except OSError as e:
            self.logger.warning(f'Copying failed: {e}')
            raise OSError()

    def get_previous_times(self):
        try:
            with self.poll_time_file.open('r') as stream:
                return json.load(stream)
        except (OSError, ValueError) as e:
            self.logger.warning(f'No previous times found: {e}')
            return {'poll': 0, 'sample': 0}

    def set_previous_times(self, times):
        try:
            with self.poll_time_file.open('w') as stream:
                json.dump(times, stream)
        except OSError as e:
            self.logger.warning(f'Failed to update poll time: {e}')

    def poll_group_path(self, group_path, current_times, prev_times):
        try:
            group_key = group_path.name
            group_name = GROUP_MAPPINGS[group_key]
        except KeyError:
            # self.logger.warning(f'"{group_key}" not defined in "group_mappings"')
            # self.logger.warning(f'Could not poll {group_path}')
            return
        try:
            self.logger.debug(f'Polling {group_name}')
            for inp_path in group_path.iterdir():
                mtime = inp_path.stat().st_mtime_ns
                if mtime < prev_times['sample']:
                    continue
                if mtime == prev_times['sample']:
                    for file in inp_path.rglob('*'):
                        if file.is_dir():
                            continue
                        if file.stat().st_mtime_ns > prev_times['poll']:
                            break
                    else:
                        continue
                elif mtime > current_times['sample']:
                    current_times['sample'] = mtime
                out_path = self.out_root_path / group_name / self.spect_name / inp_path.name
                out_path.parent.mkdir(exist_ok=True, parents=True)
                self.copy_data(inp_path, out_path)
        except OSError as e:
            self.logger.warning(f'Could not poll {group_path}')
            raise OSError()

    def poll_spect_path(self):
        try:
            self.logger.debug(f'Starting poll')
            prev_times = self.get_previous_times()
            current_times = prev_times.copy()
            current_times['poll'] = round(datetime.now().timestamp()*1e9)
            for group_path in self.inp_root_path.iterdir():
                self.poll_group_path(group_path, current_times, prev_times)
            self.logger.debug(f'Poll done')
            self.set_previous_times(current_times)
        except OSError as e:
            self.logger.warning(f'Poll failed: {e}')

    def __call__(self):
        try:
            while True:
                self.poll_spect_path()
                time.sleep(self.poll_timeout)
        except KeyboardInterrupt:
            return


if __name__ == '__main__':
    logging.debug(f'Running ftpnmr version {__version__}')
    target_root_path = mount(TARGET_ROOT)
    process_list = []
    for src_root in SRC_ROOT_LIST:
        src_root_path = mount(src_root)
        spect_name = src_root_path.name
        try:
            handler = SpectHandler(src_root_path, target_root_path, poll_timeout=POLL_TIMEOUT)
            process = Process(target=handler)
            process_list.append(process)
        except RuntimeError:
            logging.warning(f'Failed to initialize an handler for {src_root_path}')
    try:
        for process in process_list:
            process.start()
        while True:
            time.sleep(POLL_TIMEOUT)
    except KeyboardInterrupt:
        for process in process_list:
            process.terminate()
            process.join()
